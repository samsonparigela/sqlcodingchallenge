CREATE DATABASE SQLCodingChallenge;

CREATE TABLE customers
(
    customer_id INT PRIMARY KEY,
    name VARCHAR(50),
    email VARCHAR(50),
    password VARCHAR(50)
);

CREATE TABLE products
(
    product_id INT PRIMARY KEY,
    name VARCHAR(50),
    DESCRIPTION VARCHAR(50),
    PRICE FLOAT,
    stockQuantity INT,
    category VARCHAR(50)
);

CREATE TABLE cart
(
    cart_id INT PRIMARY KEY,
    customer_id INT,
    FOREIGN KEY(customer_id) REFERENCES customers(customer_id),
    product_id INT,
    FOREIGN KEY(product_id) REFERENCES products(product_id),
    quantity INT
);

CREATE TABLE orders
(
    order_id INT Primary Key,
    customer_id INT,
    Foreign Key(customer_id) REFERENCES customers(customer_id),
    order_date DATE,
    total_price FLOAT
);

CREATE TABLE order_items
(
    order_item_id INT PRIMARY KEY,
    order_id INT,
    FOREIGN KEY(order_id) REFERENCES orders(order_id),
    product_id INT,
    FOREIGN KEY(product_id) REFERENCES products(product_id),
    quantity INT,
    total_amount FLOAT
);


INSERT INTO products VALUES
(1,'Laptop','High-performance laptop','electronic',800.00,10),
(2,'Smartphone','Latest smartphone','electronic',600.00,15),
(3,'Tablet','Portable tablet','electronic',300.00,20),
(4,'Headphones','Noise-canceling','electronic',150.00,30),
(5,'TV','4K Smart TV ','electronic',900.00,5),
(6,'Coffee Maker','Automatic coffee maker','home appliance',50.00,25),
(7,'Refrigerator','Energy-efficient','home appliance',700.00,10),
(8,'Microwave Oven','Countertop microwave','home appliance',80.00,15),
(9,'Blender','High-speed blender','home appliance',70.00,20),
(10,'Vacuum Cleaner','r Bagless vacuum cleaner','home appliance',120.00,10);

INSERT INTO customers VALUES
(1,'John Doe','johndoe@example.com','password123'),
(2,'Jane Smith','janesmith@example.com','password123'),
(3,'Robert Johnson','robert@example.com','password123'),
(4,'Sarah Brown','sarah@example.com','password123'),
(5,'David Lee','david@example.com','password123'),
(6,'Laura Hall','laura@example.com','password123'),
(7,'Michael Davis','michael@example.com','password123'),
(8,'Emma Wilson','emma@example.com','password123'),
(9,'William Taylor','william@example.com','password123'),
(10,'Olivia Adams','olivia@example.com','password123');

INSERT INTO orders VALUES
(1,1,'2023/01/05',1200.00),
(2,2,'2023/02/10',900.00),
(3,3,'2023/03/15',300.00),
(4,4,'2023/04/20',150.00),
(5,5,'2023/05/25',1800.00),
(6,6,'2023/06/30',400.00),
(7,7,'2023/07/05',700.00),
(8,8,'2023/08/10',160.00),
(9,9,'2023/09/15',140.00),
(10,10,'2023/10/20',1400.00);

INSERT INTO order_items VALUES
(1,1,1,2,1600.00),
(2,1,3,1,300.00),
(3,2,2,3,1800.00),
(4,3,5,2,1800.00),
(5,4,4,4,600.00),
(6,4,6,1,50.00),
(7,5,1,1,800.00),
(8,5,2,2,1200.00),
(9,6,10,2,240.00),
(10,6,9,3,210.00);

INSERT INTO cart VALUES
(1,1,1,2),
(2,1,3,1),
(3,2,2,3),
(4,3,4,4),
(5,3,5,2),
(6,4,6,1),
(7,5,1,1),
(8,6,10,2),
(9,6,9,3),
(10,7,7,2);

SELECT * FROM cart;
SELECT * FROM customers;
SELECT * FROM orders;
SELECT * FROM order_items;
SELECT * FROM products;




/*
1. Update refrigerator product price to 800.
*/

UPDATE products
SET price=800
WHERE name like 'Refrigerator';


/*
2. Remove all cart items for a specific customer.
*/

DECLARE @X INT;
SET @X=3;

DELETE FROM cart
WHERE customer_id=@X;


/*
3. Retrieve Products Priced Below $100.
*/

SELECT * FROM products
WHERE PRICE<100;


/*
4. Find Products with Stock Quantity Greater Than 5.
*/

SELECT * FROM products
WHERE stockQuantity>5;

/*
5. Retrieve Orders with Total Amount Between $500 and $1000.
*/

SELECT * FROM orders
WHERE total_price BETWEEN 500 AND 1000;

/*
6. Find Products which name end with letter ‘r’.
*/

SELECT * FROM products
WHERE name like '%r';

/*
7. Retrieve Cart Items for Customer 5.
*/


SELECT name FROM products WHERE product_id IN
(
    SELECT PRODUCT_ID FROM cart
    WHERE customer_id=5
)


/*
8. Find Customers Who Placed Orders in 2023.
*/


SELECT CUSTOMER_ID FROM ORDERS
WHERE YEAR(order_date)=2023;




/*
9. Determine the Minimum Stock Quantity for Each Product Category.
*/

SELECT category,MIN(stockQuantity) AS MIN
FROM products
GROUP BY category;

/*
10. Calculate the Total Amount Spent by Each Customer.
*/

SELECT customer_id,SUM(total_price) AS Total_Amount
FROM orders
GROUP BY customer_id;

/*
11. Find the Average Order Amount for Each Customer.
*/
    SELECT customer_id,AVG(total_price) AS AVG_ORDER_AMOUNT
    FROM orders
    GROUP BY customer_id;
/*
12. Count the Number of Orders Placed by Each Customer.
*/

SELECT CUSTOMER_ID,COUNT(customer_id) AS NO_OF_ORDERS FROM orders
GROUP BY customer_id;

/*
13. Find the Maximum Order Amount for Each Customer.
*/


SELECT C.NAME,MAX(O.total_price) AS MAX_ORDER_AMOUNT
FROM customers C
JOIN orders O
ON C.customer_id=O.order_id
GROUP BY C.name;



/*
14. Get Customers Who Placed Orders Totaling Over $1000
*/
SELECT customer_id,name FROM customers
WHERE customer_id IN
(
    SELECT customer_id FROM orders
    WHERE TOTAL_PRICE>1000
)


/*
15. Subquery to Find Products Not in the Cart.
*/

SELECT product_id,name FROM products
WHERE product_id NOT IN
(
    SELECT P.product_id FROM PRODUCTS P
    JOIN CART C
    ON C.product_id=P.product_id
);



/*
16. Subquery to Find Customers Who Haven't Placed Orders.
*/


SELECT name FROM CUSTOMERS
WHERE customer_id NOT IN
(
    SELECT c.customer_id FROM customers C
    JOIN orders O
    ON C.customer_id=O.customer_id
);

/*
17. Subquery to Calculate the Percentage of Total Revenue for a Product.
*/

SELECT product_id,name,price,(price/(SELECT SUM(price) from products)*100) AS Revenue_Percentage 
from products;

/*
18. Subquery to Find Products with Low Stock.
*/


SELECT * FROM products
WHERE stockQuantity=
(
    SELECT MIN(stockQuantity) FROM products
)


/*
19. Subquery to Find Customers Who Placed High-Value Orders.
*/

SELECT customer_id,name FROM customers
WHERE customer_id IN
(
    SELECT customer_id FROM orders
    WHERE TOTAL_PRICE=
    (
        SELECT MAX(total_price) FROM orders
    )
)


